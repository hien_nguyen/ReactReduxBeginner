// ES6: javascript module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YoutubeSearch from 'youtube-api-search';

import VideoList from './components/video_list';
import SearchBar from './components/search_bar';
import VideoDetail from "./components/video_detail";

const YOUTUBE_API_KEY = 'AIzaSyCjGGy27aWMFQZsEKc4_LV3rzzCH2VbO1g';

/* STEP 1: New Component */
// Create a new component, produces some HTML
// const: ES6 concept = var + final + constant.
/* const App = () => {
  //JSX JSX JSX -> produces REACT "elements"
  return (
    <div>
      <SearchBar />
    </div>
  );
}; */

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      videos : [],
      selectedVideo : null
    };

    YoutubeSearch({ key : YOUTUBE_API_KEY, term: 'surfboards'}, (videos) => {
      this.setState({ 
        videos : videos,
        selectedVideo : videos[0]
      });
      // This is ES6 === videos : videos;
    });
  }

  render() {
    return (
      <div>
        <SearchBar />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList videos={this.state.videos} />
      </div>
    );
  }
}

// Take this and put it in the DOM
ReactDOM.render(<App />, document.querySelector('.container'));
