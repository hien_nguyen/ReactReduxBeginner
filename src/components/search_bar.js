// ES6 syntax for {Component}
// { Component } === const Component = React.Component;
import React, { Component } from 'react';

// ES6 syntax for
// Each class-based component has STATE
// if STATE CHANGED, the function will be RE-RENDER
class SearchBar extends Component {
  // Initialize STATE in the CLASS-BASED component by defining constructor method
  constructor(props) {
    super(props);

    // state of this CLASS-BASED component, Map<Key,Value>
    this.state = {
      term: ''
    };
  }

  // render the HTML String to React Class-based component
  render() {
    return (
      <input onChange={this.onInputChange} />
    );
  }

  onInputChange(event) {
    this.setState({
      term: event.target.value
    });
  }
}

export default SearchBar;
